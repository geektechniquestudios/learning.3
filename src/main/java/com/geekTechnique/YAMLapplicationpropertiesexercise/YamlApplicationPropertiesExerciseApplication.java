package com.geekTechnique.YAMLapplicationpropertiesexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YamlApplicationPropertiesExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(YamlApplicationPropertiesExerciseApplication.class, args);
	}

}
