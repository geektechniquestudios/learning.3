package com.geekTechnique.YAMLapplicationpropertiesexercise;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PageController {

    @Value("${app.description}")
    private String messageFromAppProp;

    @Value("${pageController.msg}")
    private String pageControllerMsg;

    @RequestMapping("/")
    public String home(){
        return messageFromAppProp;
    }

}
